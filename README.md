# threejs

#### 介绍
学习three.js-汽车案例，因为 three.js 是3D框架，api多且复杂，了解即可 

###

![https://gitee.com/buyaoyangzhu/threejs/raw/master/preview.png](https://gitee.com/buyaoyangzhu/threejs/raw/master/preview.png)

https://tanzizhou.fun/three/
如果浏览看效果，因为 3D建模 文件较大，预计打开需要 `1` 分钟，请耐心等待

#### 软件架构
three.js - 3D展示框架


#### 安装教程

1.  npm i
2.  npm start

#### 简要说明
Three.js就是模拟真实的3D场景，由以下核心组成
1.  场景Scene(场景布置，布置下面所有内容和展示)
2.  相机Camera(类似人的眼睛视觉，站在哪里看)
3.  网格模型Mesh(构建物体)
4.  渲染器Renderer(把场景渲染到页面上)
5.  其它 - 坐标轴，控制器，光线，3D建模(汽车模型)，动画(tween.js)
