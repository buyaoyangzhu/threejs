
import * as THREE from 'three'

import TWEEN from '@tweenjs/tween.js';
import { RectAreaLightHelper } from 'three/examples/jsm/helpers/RectAreaLightHelper'

import Lamborghini from './public/Lamborghini.glb'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

import GUI from 'lil-gui';

import qq from './public/qq.png'

let width = document.body.clientWidth; //宽度
let height = document.body.clientHeight; //高度
let scene, camera, renderer, controls, mesh; // 场景
let rectLight1,rectLight2,rectLight3 //灯光
let doors = [] // 车门
let carStatus = 'close'; // 车门状态

// 车身材质
let bodyMaterial = new THREE.MeshPhysicalMaterial({
    color: "#6aa4e2",
    metalness: 1,
    roughness: 0.5,
    clearcoat: 1.0,
    clearcoatRoughness: 0.03,
});


// 玻璃材质
let glassMaterial = new THREE.MeshPhysicalMaterial({
    color: "#dddddd",
    metalness: 0.25,
    roughness: 0,
    transmission: 1.0 //透光性.transmission属性可以让一些很薄的透明表面，例如玻璃，变得更真实一些。
});


// 初始化场景
function initScene() {
    scene = new THREE.Scene()
    scene.add(new THREE.AxesHelper(3))
}

// 初始化相机
function initCamera() {
    camera = new THREE.PerspectiveCamera(40, width / height, 0.1, 300)
    camera.position.set(3, 1.5, -6.5);
}


// 首次渲染
function initRenderer() {
    renderer = new THREE.WebGLRenderer({
        antialias: true
    })
    renderer.setSize(width, height)
    // 支持阴影
    renderer.shadowMap.enabled = true
    document.body.appendChild(renderer.domElement)
}


// 绘制地面网格
function iniTGridHelper() {
    let grid = new THREE.GridHelper(20, 20, 'red', 0xffffff);
    grid.material.opacity = 0.2
    grid.material.transparent = true
    scene.add(grid)
}

// 加载汽车
function loadCarModal() {
    new GLTFLoader().load(Lamborghini, function (gltf) {
        const carModel = gltf.scene
        carModel.rotation.y = Math.PI

        carModel.traverse(obj => {
            if (obj.name === 'Object_103' || obj.name == 'Object_64' || obj.name == 'Object_77') {
                // 车身
                obj.material = bodyMaterial

            } else if (obj.name === 'Object_90') {
                // 玻璃
                obj.material = glassMaterial
            } else if (obj.name === 'Empty001_16' || obj.name === 'Empty002_20') {
                // 门
                doors.push(obj)
            } else {


            }
            obj.castShadow = true;
        })
        scene.add(carModel)
    })
}

// 加载环境光
function initAmbientLight() {
    scene.add(new THREE.AmbientLight('#fff', 1))
}


// 地板
function initFloor() {
    const floorGeometry = new THREE.PlaneGeometry(20, 20)
    const material = new THREE.MeshPhysicalMaterial({
        side: THREE.DoubleSide,
        color: 0x808080,
        metalness: 0,
        roughness: 0.1
    })

    const mesh = new THREE.Mesh(floorGeometry, material)
    mesh.rotation.x = Math.PI / 2
    mesh.receiveShadow = true;
    scene.add(mesh)
}

// 添加头顶聚光灯
function initSpotLight() {
    const spotLight = new THREE.SpotLight("#ffffff", 2);

    spotLight.angle = Math.PI / 8; //散射角度，跟水平线的家教
    spotLight.penumbra = 0.2;  // 聚光锥的半影衰减百分比
    spotLight.decay = 0.2; // 纵向：沿着光照距离的衰减量。
    spotLight.distance = 30; // 光最远能照到哪
    spotLight.shadow.radius = 10;
    // 阴影映射宽度，阴影映射高度
    spotLight.shadow.mapSize.set(4096, 4096);

    // 光的位置
    spotLight.position.set(-5, 10, 1);
    // 光照射的方向
    spotLight.target.position.set(0, 0, 0);
    spotLight.castShadow = true;
    // spotLight.map = bigTexture
    scene.add(spotLight);
}

// 添加环境圆柱体
function initCylinder() {
    const geometry = new THREE.CylinderGeometry(12, 12, 20, 32)
    const material = new THREE.MeshPhysicalMaterial({
        color: 0x6c6c6c,
        side: THREE.DoubleSide
    })

    const cylinder = new THREE.Mesh(geometry, material)
    scene.add(cylinder)
}

// 坐标控制器
function initController() {
    controls = new OrbitControls(camera, renderer.domElement)

    controls.enableDamping = true

    controls.maxDistance = 9
    controls.minDistance = 1

    controls.minPolarAngle = 0
    controls.maxPolarAngle = 80 / 360 * 2 * Math.PI

    // controls.target.set(0, 0.5, 0)

}

function initGUI() {
    var obj = {
        bodyColor: '#6aa4e2',
        glassColor: '#dddddd',
        carOpen,
        carClose,
        carIn,
        carOut
    };

    const gui = new GUI();
    gui.addColor(obj, "bodyColor").name('车身颜色').onChange((value) => {
        bodyMaterial.color.set(value)
    })

    gui.addColor(obj, "glassColor").name('玻璃颜色').onChange((value) => {
        glassMaterial.color.set(value)
    })
    gui.add(obj, "carOpen").name('打开车门')
    gui.add(obj, "carClose").name('关门车门')

    gui.add(obj, "carIn").name('车内视角')
    gui.add(obj, "carOut").name('车外视角')
}

function carOpen() {
    if(carStatus === 'open') return
    carStatus = 'open'
    for (let i = 0; i < doors.length; i++) {
        setAnimationDoor({ x: 0 }, { x: Math.PI / 3 }, doors[i])
    }
}

function carClose() {
    if(carStatus === 'close') return
    carStatus = 'close'
    for (let i = 0; i < doors.length; i++) {
        setAnimationDoor({ x: Math.PI / 3 }, { x: 0 }, doors[i])
    }
}

function carIn() {
    setAnimationCamera({ x: -0.3, y: 0.8, z: 0.6, tx: 0, ty: 0.5, tz: -2 });
}

function carOut() {
    setAnimationCamera({ x: 3, y: 1.5, z: -6.5, tx: 0, ty: 0.5, tz: 0 });
}


function setAnimationDoor(start, end, mesh) {
    const tween = new TWEEN.Tween(start).to(end, 1000).easing(TWEEN.Easing.Quadratic.Out)
    tween.onUpdate((that) => {
        mesh.rotation.x = that.x
    })
    tween.start()
}

// 视觉动画
function setAnimationCamera(end) {
    const tween = new TWEEN.Tween({
        // 不管相机此刻处于什么状态，直接读取当前的位置和目标观察点
        x: camera.position.x,
        y: camera.position.y,
        z: camera.position.z,
        tx: controls.target.x,
        ty: controls.target.y,
        tz: controls.target.z,
    }).to({
        // 动画结束相机位置坐标
        x: end.x,
        y: end.y,
        z: end.z,
        // 动画结束相机指向的目标观察点
        tx: end.tx,
        ty: end.ty,
        tz: end.tz,
    }, 3000).easing(TWEEN.Easing.Quadratic.Out)
    tween.onUpdate(function (obj) {
        // 动态改变相机位置
        camera.position.set(obj.x, obj.y, obj.z);
        // 动态计算相机视线
        camera.lookAt(obj.tx, obj.ty, obj.tz);
        controls.target.set(obj.tx, obj.ty, obj.tz);
    })
    
    tween.onComplete(function (that) {
        camera.position.set(end.x, end.y, end.z);
        controls.target.set(end.tx, end.ty, end.tz);
    })
    tween.start()
}

// 创建聚光灯函数
function createSpotlight(color) {
    const newObj = new THREE.SpotLight(color, 10);
    newObj.castShadow = true;
    newObj.angle = Math.PI / 20;
    newObj.penumbra = 0.2;
    newObj.decay = 0.2;
    newObj.distance = 30;
    return newObj;
}

// 创建聚光灯和纹理
function initMessiLight() {
    const spotLight1 = createSpotlight('#ffffff');
    const texture = new THREE.TextureLoader().load(qq)

    spotLight1.position.set(0, 4, 0);
    spotLight1.target.position.set(-4, 3, 4)

    spotLight1.map = texture
    const lightHelper1 = new THREE.SpotLightHelper(spotLight1);
    scene.add(spotLight1);
}

function initMutilColor() {
    //创建三色光源
    rectLight1 = new THREE.RectAreaLight(0xff0000, 50, 1, 10);
    rectLight1.position.set(15, 10, 15);
    rectLight1.rotation.x = -Math.PI / 2
    rectLight1.rotation.z = -Math.PI / 4
    scene.add(rectLight1);


    rectLight2 = new THREE.RectAreaLight(0x00ff00, 50, 1, 10);
    rectLight2.position.set(13, 10, 13);
    rectLight2.rotation.x = -Math.PI / 2
    rectLight2.rotation.z = -Math.PI / 4
    scene.add(rectLight2);


    rectLight3 = new THREE.RectAreaLight(0x0000ff, 50, 1, 10);
    rectLight3.position.set(11, 10, 11);
    rectLight3.rotation.x = -Math.PI / 2
    rectLight3.rotation.z = -Math.PI / 4
    scene.add(rectLight3);

    scene.add(new RectAreaLightHelper(rectLight1));
    scene.add(new RectAreaLightHelper(rectLight2));
    scene.add(new RectAreaLightHelper(rectLight3));


    startColorAnim()
}

function startColorAnim() {
    const carTween = new TWEEN.Tween({ x: -5 }).to({ x: 25 }, 2000).easing(TWEEN.Easing.Quadratic.Out);
    carTween.onUpdate(function (that) {
        rectLight1.position.set(15 - that.x, 10, 15 - that.x)
        rectLight2.position.set(13 - that.x, 10, 13 - that.x)
        rectLight3.position.set(11 - that.x, 10, 11 - that.x)
    });
    carTween.onComplete(function (that) {
        rectLight1.position.set(-15, 10, 15);
        rectLight2.position.set(-13, 10, 13);
        rectLight3.position.set(-11, 10, 11);

        rectLight1.rotation.x = Math.PI / 4
        rectLight2.rotation.x = Math.PI / 4
        rectLight3.rotation.x = Math.PI / 4
    })
    carTween.repeat("Infinity")
    carTween.start();


    const carTween2 = new TWEEN.Tween({ x: -5 }).to({ x: 25 }, 2000).easing(TWEEN.Easing.Quadratic.Out);
    carTween2.onUpdate(function (that) {
        rectLight1.position.set(-15 + that.x, 10, 15 - that.x)
        rectLight2.position.set(-13 + that.x, 10, 13 - that.x)
        rectLight3.position.set(-11 + that.x, 10, 11 - that.x)
    });
    carTween2.onComplete(function (that) {
        rectLight1.position.set(15, 10, 15);
        rectLight2.position.set(13, 10, 13);
        rectLight3.position.set(11, 10, 11);
        rectLight1.rotation.x = - Math.PI / 4
        rectLight2.rotation.x = - Math.PI / 4
        rectLight3.rotation.x = - Math.PI / 4
    })
    carTween2.repeat("Infinity")
    carTween2.start()
}

function init() {
    initScene()
    initCamera()
    initRenderer()
    loadCarModal()
    initAmbientLight()
    iniTGridHelper()
    initFloor()
    initSpotLight()

    initCylinder()
    initController()

    initGUI()
    initMessiLight()
    initMutilColor()
}

init()


function render() {
    renderer.render(scene, camera)
    requestAnimationFrame(render)

    controls.update()
    // TWEEN动画库
    TWEEN.update();
}

render()

window.onresize = () => {
    width = document.body.clientWidth; //宽度
    height = document.body.clientHeight; //高度

    // 相机
    camera.aspect = width / height;
    camera.updateProjectionMatrix()

    // 画布
    renderer.setSize(width, height)
}

// 使用计算射线获取鼠标点击的模型
window.addEventListener('click', onPointClick);
function onPointClick(event) {
    let pointer = {}
    pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    pointer.y = - (event.clientY / window.innerHeight) * 2 + 1;


    var vector = new THREE.Vector2(pointer.x, pointer.y)
    var raycaster = new THREE.Raycaster()
    raycaster.setFromCamera(vector, camera)
    let intersects = raycaster.intersectObjects(scene.children);

    intersects.forEach((item) => {
        if (item.object.name === 'Object_64' || item.object.name === 'Object_77') {
            if (carStatus === 'close') {
                carOpen()
            } else {
                carClose()
            }
        }
    })


}