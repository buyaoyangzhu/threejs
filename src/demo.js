
import * as THREE from 'three'
import TWEEN from '@tweenjs/tween.js';
import qq from './public/qq.png'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
let geometry, material, mesh, scene, width, height, camera, renderer, controls,texture;
const init = () => {
    //创建一个长方体几何对象Geometry
    geometry = new THREE.BoxGeometry(200, 200, 200);

    texture = new THREE.TextureLoader()
    //创建一个材质对象Material
    material = new THREE.MeshBasicMaterial({
        color: 'white', //设置材质颜色
        transparent: true, //开启透明
        opacity: 0.5, //设置透明度
        map: texture.load(qq)
    });

    // 两个参数分别为几何体geometry、材质material
    mesh = new THREE.Mesh(geometry, material); //网格模型对象Mesh
    //设置网格模型在三维空间中的位置坐标，默认是坐标原点
    mesh.position.set(100,100,100);

    scene = new THREE.Scene();
    scene.add(mesh);

    // AxesHelper：辅助观察的坐标系
    axesHelper = new THREE.AxesHelper(200);
    scene.add(axesHelper);

    // width和height用来设置Three.js输出的Canvas画布尺寸(像素px)
    width = document.body.clientWidth; //宽度
    height = document.body.clientHeight; //高度
    // 30:视场角度, width / height:Canvas画布宽高比, 1:近裁截面, 3000：远裁截面
    camera = new THREE.PerspectiveCamera(60, width / height, 1, 3000);
    //相机在Three.js三维坐标系中的位置
    // 根据需要设置相机位置具体值
    camera.position.set(0,0,1000);
    camera.lookAt(0,0,0);

    pointLight = new THREE.PointLight(0xffffff, 2.0);
    pointLight.position.set(100, 100, 50); //点光源放在x轴上

    scene.add(pointLight); //点光源添加到场景中

    // 创建渲染器对象
    renderer = new THREE.WebGLRenderer({antialias: true});

    renderer.setSize(width, height); //设置three.js渲染区域的尺寸(像素px)
    renderer.render(scene, camera); //执行渲染操作

    //   轨道控制器
    controls = new OrbitControls(camera, renderer.domElement)

    document.body.appendChild(renderer.domElement);
    animation()
    render();
};

const animation = () => {
    //创建一段mesh平移的动画
    const tween = new TWEEN.Tween(mesh.position);
    //经过2000毫秒，pos对象的x和y属性分别从零变化为100、50
    tween.to({x: 100,y: 50}, 2000);
    //tween动画开始执行
    tween.start(); 
}

const render = () => {
    // TWEEN动画库
    TWEEN.update();
    renderer.render(scene, camera); //执行渲染操作
    requestAnimationFrame(render);
};

init();


window.onresize = () => {
    width = document.body.clientWidth; //宽度
    height = document.body.clientHeight; //高度

    // 相机
    camera.aspect = width / height;
    camera.updateProjectionMatrix()

    // 画布
    renderer.setSize(width,height)
}