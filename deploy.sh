#!/usr/bin/env sh

# 确保脚本抛出遇到的错误
set -e

rm -f-r dist/

# 生成静态文件
npm run build

# 提交代码即可
git add .
git commit -m 'deploy'
git push

cd -